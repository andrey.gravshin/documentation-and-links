# Docker



# Kibana
Огромная, полезная статья: https://coralogix.com/blog/managing-docker-logs-with-elk-and-fluentd/

Что такое индексы: https://www.elastic.co/blog/what-is-an-elasticsearch-index

# Elasticsearch
https://www.elastic.co/guide/en/elasticsearch/reference/8.3/docker.html#docker-compose-file

# Docker образы

docker images | grep -v "minikube" | awk '{print $3}' | xargs docker rmi -f # Удаляет все докер-образы за исключением minikube

https://hub.docker.com/r/chentex/random-logger

`docker run -d --rm --name testlogs chentex/random-logger:v1.0.1`

https://hub.docker.com/r/fabxc/instrumented_app

`docker run -d --rm --name testmetrics fabxc/instrumented_app`

Loki логи (есть условия запуска по ссылке ниже)
https://github.com/cyriltovena/logger

`docker run ctovena/logger:0.12 --url=http://localhost:3100/api/prom/push`

# VSCode
Как установить терминал по умолчанию
CTRL + SHIFT + P

Terminal: Select Default Profile

Выбрать например GitBash